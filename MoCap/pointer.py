import sys
import pygame
import time
import numpy as np

from ws_gesture_client import LeapInterface

from utility import *
from view import *
from image_picker import *
from input_controller import *

import threading


def main():
    pygame.init()

    leap_if = LeapInterface(url="ws://localhost:6437")
    leap_if.start()

    size = width, height = 2*640, 2*480

    main_screen = pygame.display.set_mode(size)
    pygame.display.set_caption('Brainstormer')
    clock = pygame.time.Clock()

    vc = ViewController(main_screen)

    dateNode = TextNode((width - 10, 10), "12.12.2019", color=Colors.BLACK, alignment=TextAlignment.RIGHT)
    vc.addchild(dateNode)

    img_picker_thread = threading.Thread(target=control_loop, args=(vc, leap_if, ))
    img_picker_thread.start()


    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                break
        
        clock.tick(30)
        main_screen.fill(Colors.WHITE)

        vc.draw()

        pygame.display.update()

    img_picker_thread.join()


def visualize_fingers():
    pygame.init()

    leap_if = LeapInterface(url="ws://localhost:6437")
    leap_if.start()

    size = width, height = 2*640, 2*480
    main_screen = pygame.display.set_mode(size)

    clock = pygame.time.Clock()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        main_screen.fill(Colors.BLACK)
        
        clock.tick(30)

        poss = leap_if.pointing_position
        if poss != None:
            for pos in poss:
                blue = np.clip(int(256 + pos[2]), 0, 255)
                red = np.clip(int(256 - pos[2]), 0, 255)
                c = (red, 0, blue)
                pygame.draw.circle(main_screen, c, (pos[0] + (width//2), height - pos[1]), 10)

        pygame.display.update()


if __name__ == "__main__":
    main()