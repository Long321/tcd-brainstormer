#!/usr/bin/env python3

from view import *
from utility import *
from urllib.error import HTTPError


class ImagePicker(RectNode):
    def __init__(self, pos, image_url_list):
        super(ImagePicker, self).__init__(pos, (560, 240), color=Colors.LIGHT_GRAY)
        self.image_url_list = image_url_list
        self.refresh_count = 0

        label = TextNode((self.size[0] // 2, 20), "Choose an image", fontsize=35, color=Colors.BLACK)
        label.alignment = TextAlignment.CENTER
        label.tag = "label"
        self.addchild(label)
        self.get_images_for_urls()

    def get_images_for_urls(self):
        if self.refresh_count >= 3:
            return
        self.refresh_count += 1

        for child in self.children:
            if child.tag != "label":
                # TODO: idx doesn't get deleted
                self.removechild(child)

        for child in self.children:
            print(child.tag)
            
        img_width = 160
        url_idx = 0
        idx = 0
        while len(self.children) < 4:
            url = self.image_url_list[url_idx]
            try:
                wi = WebImage(url, pos=(20 + idx * (img_width + 20), 60), size=(160, 160))

            except HTTPError:
                url_idx += 1
                continue
            wi.tag = idx
            self.addchild(wi)
            url_idx += 1
            idx += 1
        
        self.image_url_list = self.image_url_list[url_idx:]