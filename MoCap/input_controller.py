#!/usr/bin/env python3

import sys
import pygame
import time
import numpy as np

from ws_gesture_client import LeapInterface

from utility import *
from view import *
from image_picker import *

import threading
sys.path.append('../google-images/src/')
from speech_to_image import sti_parser
# color_list = Colors.get_all_colors()

def get_pos_in_view(viewCon, pos):
    width, height = viewCon.screen.get_size()
    def map_value(v, fl, fu, tl, tu):
        v = min(max(v, fl), fu)
        return ((v - fl) / (fu - fl)) * (tu - tl) + tl
    x = map_value(pos[0], -100, 100, 0, width)
    y = map_value(pos[1], 50, 300, height, 0)
    # touch_in_view = Point(x, 480 - 2 * pos[1])
    return Point(x, y)


def add_image(viewCon, leap_if):
    """
    :param viewCon: main view controller
    :param leap_if: leap motion interface
    """
    
    print("adding_image")
    width, height = viewCon.screen.get_size()
    center = Point(width//2, height//2)
    
    listening_label = TextNode(center, "Adjusting to Background Noise", 30, color=Colors.RED, alignment=TextAlignment.CENTER)
    viewCon.addchild(listening_label)

    # create speech to image parser

    sti = sti_parser(img_limit=20)
    sti.adjust_noise_reduction()

    listening_label.text = "Listening... (Speech to Image)"

    input_urls = []
    while len(input_urls) == 0:
        try:
            input_urls = sti.get_image_urls_from_speech()
        except Exception as e:
            print(e)
            print('Couldnt find any images. Try again!')

    viewCon.removechild(listening_label)

    ip = ImagePicker((360, 40), input_urls)
    viewCon.addchild(ip)

    pointer = CircleNode((-20, -20), 10, color=Colors.RED)
    viewCon.addchild(pointer)

    print("Select image")

    chosen_img = None 
    while chosen_img is None:
        time.sleep(0.03)

        poss = leap_if.pointing_position
        if poss is None:
            continue 

        pos = poss[0]

        touch_in_view = get_pos_in_view(viewCon, pos)
        pointer.position = touch_in_view
        
        if leap_if.last_key_tap > time.time() - 0.1:
            leap_if.last_key_tap = 0
            ip.get_images_for_urls()

        if leap_if.last_screen_tap > time.time() - 0.1:
            leap_if.last_screen_tap = 0
            # trigger selection
            for child in ip.children:
                if type(child) != WebImage:
                    continue

                if child.isPointInside(touch_in_view):
                    chosen_img = child
                    chosen_img.use_thumbnail = False

    pointer.removeFromParent()

    viewCon.addchild(chosen_img)
    
    viewCon.removechild(ip)

    while True:
        time.sleep(0.03)

        poss = leap_if.pointing_position
        if poss is None:
            continue

        pos = poss[0]

        # touch_in_view = Point(2 * pos[0] + (640 // 2), 480 - 2 * pos[1])
        touch_in_view = get_pos_in_view(viewCon, pos)
        chosen_img.position = touch_in_view - Point(chosen_img.size[0], chosen_img.size[1])
        
        if leap_if.last_screen_tap > time.time() - 0.1:
            leap_if.last_screen_tap = 0
            break


def add_text(viewCon, leap_if):
    print("adding text")
    width, height = viewCon.screen.get_size()
    center = Point(width//2, height//2)

    listening_label = TextNode(center, "Adjusting to Background Noise", 30, color=Colors.RED, alignment=TextAlignment.CENTER)
    viewCon.addchild(listening_label)

    sti = sti_parser(img_limit = 20)
    sti.adjust_noise_reduction()

    listening_label.text = "Listening... (Speech to Text)"

    # listen and save word in variable
    word = sti.get_word()
    listening_label.removeFromParent()

    tl = TextNode((50, -50), word, alignment=TextAlignment.CENTER)
    viewCon.addchild(tl)

    color_list = [Colors.BLACK, Colors.RED, Colors.GREEN, Colors.BLUE, Colors.LIGHT_GRAY]
    color_idx = 0

    tl.color = color_list[color_idx]

    while True:
        time.sleep(0.03)
        poss = leap_if.pointing_position
        if poss is None:
            continue

        pos = poss[0]

        # finger_in_view = Point(2 * pos[0] + (640 // 2), 480 - 2 * pos[1])
        finger_in_view = get_pos_in_view(viewCon, pos)
        tl.position = finger_in_view

        if len(poss) >= 2:
            fontsize = abs(poss[0][0] - poss[1][0])
            tl.set_fontsize(fontsize)

        if leap_if.last_screen_tap > time.time() - 0.3:
            leap_if.last_screen_tap = 0
            print("added label")
            break

        if leap_if.last_key_tap > time.time() - 0.3:
            leap_if.last_key_tap = 0
            color_idx += 1
            if color_idx >= len(color_list):
                color_idx = 0
            tl.color = color_list[color_idx]


def add_arrow(viewCon, leap_if):
    print("adding arrow")
    points = []
    ln = LineNode(points, color=Colors.BLACK)
    viewCon.addchild(ln)

    color_idx = 0
    color_list = [Colors.BLACK, Colors.RED, Colors.GREEN, Colors.BLUE, Colors.LIGHT_GRAY]
    time.sleep(0.5)
    while True:
        time.sleep(0.03)
        poss = leap_if.pointing_position
        if poss is None:
            continue
        #if len(poss) >= 2:
            #pygame.draw.lines(viewCon, color=Colors.BLACK, closed = True, (poss[0], poss[1]), width=1)
        if len(poss) >= 2:
            # p1 = Point(2 * poss[0][0] + (640 // 2), 480 - 2 * poss[0][1])
            # p2 = Point(2 * poss[1][0] + (640 // 2), 480 - 2 * poss[1][1])
            p1 = get_pos_in_view(viewCon, poss[0])
            p2 = get_pos_in_view(viewCon, poss[1])
            points = [tuple(p1), tuple(p2)]
            ln.points = points

        if leap_if.last_screen_tap > time.time() - 0.3:
            leap_if.last_screen_tap = 0
            print("added arrow")
            break
            
        if leap_if.last_key_tap > time.time() - 0.3:
            leap_if.last_key_tap = 0
            color_idx += 1
            if color_idx >= len(color_list):
                color_idx = 0
            ln.color = color_list[color_idx]


def save_screenshot(viewCon):
    print("saving screenshot")
    screen_size = viewCon.screen.get_size()
    screenshot_img = pygame.Surface(screen_size)
    screenshot_img.blit(viewCon.screen, (0, 0))
    filename = "brainstormer_" + str(time.time()).replace('.', '-') + ".png"
    pygame.image.save(screenshot_img, filename)
    w

def control_loop(viewCon, leap_if):
    """
    Main Control Loop.
    Waits for input to then decide whether the user wants an image
        or text to be displayed.
    """

    while True:
        time.sleep(0.03)
        width, height = viewCon.screen.get_size()

        poss = leap_if.pointing_position
        if poss != None:
            """
            for pos in poss:
                blue = np.clip(int(256 + pos[2]), 0, 255)
                red = np.clip(int(256 - pos[2]), 0, 255)
                c = (redf, 0, blue)
                pygame.draw.circle(viewCon.screen, c, (pos[0] + (width//2), height - pos[1]), 10)
            """

        if leap_if.last_screen_tap > time.time() - 0.3:
            leap_if.last_screen_tap = 0

            # poss = leap_if.pointing_position

            if len(poss) <= 1:
                add_image(viewCon, leap_if)
            
            elif len(poss) <= 2:
                add_text(viewCon, leap_if)
            
            elif len(poss) <= 3:
                add_arrow(viewCon, leap_if)

            elif len(poss) <= 4:
                save_screenshot(viewCon)

            time.sleep(2)

        # elif leap_if.last_swipe > time.time() - 0.3:
        #     save_screenshot(viewCon)
        #     time.sleep(1)
            