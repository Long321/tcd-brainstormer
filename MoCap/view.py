#!/usr/bin/env python3

import pygame
import io
from urllib.request import urlopen
from enum import Enum, auto

from utility import *


class TextAlignment(Enum):
        LEFT = auto()
        CENTER = auto()
        RIGHT = auto()


class ViewController:
    def __init__(self, screen):
        self.children = []
        self.screen = screen
    
    def draw(self):
        if self.screen is None:
            return

        for child in self.children:
            child.draw(self.screen, Point(0, 0))
    
    def addchild(self, new_child):
        new_child.removeFromParent()
        self.children.append(new_child)
        new_child.parent = self
    
    def removechild(self, child):
        if child in self.children:
            self.children.remove(child)
            child.parent = None
        

class Node:
    """
    Abstract class for all things that can be displayed.
    Will draw its contents and the content of its children.
    """
    def __init__(self, pos):
        """ Will initialize a Node. Converts pos of type tuple to point. """
        self.children = []
        self.parent = None
        self.tag = None
        if type(pos) == tuple:
            self.position = Point(pos)
        else:
            self.position = pos
       
    def draw(self, screen, offset):
        for child in self.children:
            child.draw(screen, self.position + offset)
    
    def addchild(self, new_child):
        new_child.removeFromParent()
        self.children.append(new_child)
        new_child.parent = self

    def removechild(self, child):
        if child in self.children:
            self.children.remove(child)
            child.parent = None

    def removeFromParent(self):
        if self.parent is None:
            return
        
        if self in self.parent.children:
            self.parent.children.remove(self)
        
        self.parent = None


class CircleNode(Node):
    """
    Represents a Circle.
    """
    def __init__(self, pos, radius, color=(100, 100, 100)):
        super(CircleNode, self).__init__(pos)
        self.radius = radius
        self.color = color
    
    def draw(self, screen, offset):
        pygame.draw.circle(screen, self.color, tuple(self.position + offset), self.radius)
        super(CircleNode, self).draw(screen, offset)
        

class RectNode(Node):
    """ Represents a Rectangle. """
    def __init__(self, pos, size, color=(100, 100, 100)):
        super(RectNode, self).__init__(pos)
        self.size = size
        self.color = color 
    
    def draw(self, screen, offset):
        r = pygame.Rect(tuple(self.position + offset), self.size)
        pygame.draw.rect(screen, self.color, r)
        super(RectNode, self).draw(screen, offset)
    

class LineNode(Node):
    """ Represents a line from various points. """
    def __init__(self, points, color=(100, 100, 100), close=False):
        super(LineNode, self).__init__((0, 0))
        self.points = points
        self.color = color
        self.close = close
    
    def draw(self, screen, offset):
        if self.points is not None and len(self.points) >= 2:
            pygame.draw.lines(screen, self.color, self.close, self.points, 3)

        super(LineNode, self).draw(screen, offset)

class TextNode(Node):
    """ Displays a text. """
    def __init__(self, pos, text, fontsize=30, color=(100, 100, 100), alignment=TextAlignment.LEFT):
        super(TextNode, self).__init__(pos)
        self.text = text
        self.color = color
        self.fontsize = fontsize
        self.alignment = alignment
        self.set_fontsize(fontsize)
    
    def set_fontsize(self, fontsize):
        self.fontsize = fontsize
        self.font = pygame.font.SysFont("Arial", self.fontsize)

    def draw(self, screen, offset):
        use_antialiasing = True
        tfield = self.font.render(self.text, use_antialiasing, self.color)
        x_pos = self.position.x
        if self.alignment == TextAlignment.RIGHT:
            x_pos -= tfield.get_size()[0]
        elif self.alignment == TextAlignment.CENTER:
            x_pos -= tfield.get_size()[0] // 2
        screen.blit(tfield, tuple(Point(x_pos, self.position.y) + offset))
        super(TextNode, self).draw(screen, offset)


class WebImage(Node):
    """ Loads an image from a given URL. """
    def __init__(self, url, pos=(0, 0), size=None):
        super(WebImage, self).__init__(pos)
        self.image = None
        self.image_thumbnail = None
        self.url = url
        self.size = size
        self.use_thumbnail = True
        self._loadImageFromWeb()

    def _loadImageFromWeb(self):
        image_str = urlopen(self.url).read()
        image_file = io.BytesIO(image_str)
        self.image = pygame.image.load(image_file)
        if self.size is not None:
            self.scale_and_crop()

    def scale_and_crop(self):
        image_size = self.image.get_rect().size
        width_ratio = image_size[0] / self.size[0]
        height_ratio = image_size[1] / self.size[1]
        downscaling = True if width_ratio > 1 or height_ratio > 1 else False
        # print('image size then self.size')
        # print(image_size)
        # print(self.size)
        if downscaling:
            scale_ratio = 1.0 / min(width_ratio, height_ratio)
        else:
            scale_ratio = max(width_ratio, height_ratio)


        scaled_image_size = tuple(int(x * scale_ratio) for x in image_size)
        # print('scaled_size')
        # print(scaled_image_size)
        self.image = pygame.transform.scale(self.image, scaled_image_size)

        size_diff = (scaled_image_size[0] - self.size[0], scaled_image_size[1] - self.size[1])
        width_offset = size_diff[0] / 2.0
        height_offset = size_diff[1] / 2.0

        cropped = pygame.Surface(self.size)
        cropped.blit(self.image, (0, 0), (width_offset, height_offset, self.size[0], self.size[1]))
        self.image_thumbnail = cropped

    def draw(self, screen, offset):
        if self.use_thumbnail:
            if self.image_thumbnail is not None:
                screen.blit(self.image_thumbnail, tuple(self.position + offset))
        else:
            if self.image is not None:
                screen.blit(self.image, tuple(self.position + offset))
        super(WebImage, self).draw(screen, offset)

    def isPointInside(self, point):
        pos = self.position
        par = self.parent
        while par != None and type(par) != ViewController:
            pos += par.position
            par = par.parent
        
        return pos.x <= point.x <= pos.x + self.size[0] and pos.y <= point.y <= pos.y + self.size[1]
       