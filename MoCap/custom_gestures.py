from operator import add
from operator import itemgetter
import numpy as np


# creates customGesture from lists of gestures e.g. multiSwipe
# speed is average speed of all gestures
class customGesture:
    def __init__(self, name, gesture_list):
        self.name = name
        self.g_list = gesture_list
        self.speed = 0
        self.direction = 'None'
        if name == 'multiSwipe':
            self.direction = self.init_direction()
        if name != 'multiCircle':
            for g in self.g_list:
                self.speed += g["speed"] / len(self.g_list)

    def init_direction(self):
        direction = 3 * [0]
        for g in self.g_list:
            direction = list(map(add, direction, g['direction']))
        index, _ = max(enumerate(map(abs, direction)), key=itemgetter(1))
        value = direction[index]
        if index == 0:
            if value > 0:
                return 'right'
            else:
                return 'left'
        if index == 1:
            if value > 0:
                return 'up'
            else:
                return 'down'
        else:
            if value > 0:
                return 'back'
            else:
                return 'front'

    def init_speed(self):
        for g in self.g_list:
            pass


# parses gesture list into a customGesture

class customGestureParser:
    def __init__(self, gesture_list):
        self.g_list = gesture_list
        self.g_types_list = [g['type'] for g in self.g_list]

    def get_custom_gestures(self):
        custom_gestures = []
        if self.g_types_list.count('keyTap') > 1:
            cg_multikeytap = customGesture('multiKeyTap', list(filter(lambda x: x['type'] == 'keyTap', self.g_list)))
            custom_gestures.append(cg_multikeytap)
        if self.g_types_list.count('circle') > 1:
            cg_multicircle = customGesture('multiCircle', list(filter(lambda x: x['type'] == 'circle', self.g_list)))
            custom_gestures.append(cg_multicircle)
        if self.g_types_list.count('screenTap') > 1:
            cg_multiscreentap = customGesture('multiScreenTap', list(filter(lambda x: x['type'] == 'screenTap', self.g_list)))
            custom_gestures.append(cg_multiscreentap)
        if self.g_types_list.count('swipe') > 1:
            cg_multiswipe = customGesture('multiSwipe', list(filter(lambda x: x['type'] == 'swipe', self.g_list)))
            custom_gestures.append(cg_multiswipe)
        return custom_gestures

