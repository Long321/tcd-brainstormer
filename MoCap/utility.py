#!/usr/bin/env python3

from enum import Enum


class Colors:

    BLACK = 0, 0, 0
    RED = (200, 0, 0)
    L_RED = (200, 102, 102)
    L_ORANGE = (255, 178, 102)
    L_YELLOW = (255, 255, 102)
    L_GREEN = (178, 255, 102)
    GREEN = (0, 200, 0)
    NEON_GREEN = (102, 255, 102)
    NEON_GREEN = (102, 255, 102)
    L_CYAN = (102, 255, 178)
    L_SKY = (102, 255, 255)
    L_BLUE = (102, 178, 255)
    BLUE = (0, 0, 200)
    L_PURPLE = (102, 102, 255)
    L_VIOLET = (178, 102, 255)
    L_PINK = (255, 102, 255)
    L_PINK2 = (255, 102, 178)
    LIGHT_GRAY = (180, 180, 180)
    WHITE = (255, 255, 255)


class Point:
    """ Simple 2D point that supports arithmetic operations. """

    def __init__(self, *args, **kwargs):
        if len(args) == 1 and type(args) == tuple:
            # initialize from tuple
            self.x = args[0][0]
            self.y = args[0][1]
        
        elif len(args) == 2:
            # initialize form two values
            self.x = args[0]
            self.y = args[1]
        
        elif "x" in kwargs.keys() and "y" in kwargs.keys():
            # initialize from dict
            self.x = kwargs["x"]
            self.y = kwargs["y"]
    
    def as_tup(self):
        """ returns the position as tuple. """
        return (self.x, self.y)
    
    def __iter__(self):
        """ conversion to a tuple is looking for an iterable. """
        yield self.x
        yield self.y

    def __str__(self):
        return f"(x: {self.x}, y: {self.y})"
    
    def __repr__(self):
        return self.__str__()
    
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    
    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)
    

def main():
    # Point class tests
    p1 = Point(1, 2)
    p2 = Point((3, 4))
    p3 = Point(y=6, x=5)

    print(p1)
    print(p2)
    print(p3)

    print(p1 + p2)

    print(tuple(p1))


if __name__ == "__main__":
    main()