import websocket
import json
from custom_gestures import customGestureParser
try:
    import thread
except ImportError:
    import _thread as thread
import time

from threading import Thread


class LeapInterface(Thread):
    """
    Resources:
        websocket msg contents: https://developer-archive.leapmotion.com/documentation/javascript/supplements/Leap_JSON.html
        -> Just click on one to see the exact definition
    """
    def __init__(self, url="ws://echo.websocket.org/"):
        super(LeapInterface, self).__init__()
        self.pointing_position = None
        self.last_screen_tap = 0
        self.last_key_tap = 0
        self.last_swipe = 0

        websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(url,
            on_message = lambda ws,msg: self.on_message(ws, msg),
            on_error   = lambda ws,msg: self.on_error(ws, msg),
            on_close   = lambda ws:     self.on_close(ws),
            on_open    = lambda ws:     self.on_open(ws))
        
    def run(self):
        self.ws.run_forever()


    def save_pointing_direction(self, j_dict):
        """
        Use the direction in which one finger points and translate it
            into x and y coordinates that are to be used inside pygame.
        Elements with index 1 and 2 from the direction vector will be used.
        """
        if len(j_dict["pointables"]) == 0:
            return
        
        pp = []
        for pointable in j_dict["pointables"]:
            tipPosition = pointable["tipPosition"]
            direction = pointable["direction"]
            
            if direction[2] > -0.68:
                # print(direction[2])
                # dont add tips that aren't looking forward
                continue
            pp.append((
                int(tipPosition[0]),
                int(tipPosition[1]),
                int(tipPosition[2])
                ))
            # if "type" in pointable.keys():
            #     # still not able to get the type of finger!?
            #     fingerType = pointable["type"]  # 0 is thumb; 4 is pinky
            #     print("fingertype:,", fingerType)

        if len(pp) == 0:
            self.pointing_position = None
            return

        pp.sort(key=lambda position: position[2])
        self.pointing_position = pp

    def print_gesture_type(self, j_dict):
        if len(j_dict["gestures"]) == 0:
            return
        
        for gesture in j_dict["gestures"]:
            
            if gesture["type"] == "screenTap":
                self.last_screen_tap = time.time()
            elif gesture["type"] == "keyTap":
                self.last_key_tap = time.time()
            elif gesture["type"] == "swipe":
                self.last_swipe = time.time()

        #for g in gesture_list:
        #    print(g['type'])
        # cg = customGestureParser(gesture_list)
        # for cg in cg.get_custom_gestures():
        #     print(cg.direction)
        # print("---")
        

    def on_message(self, ws, message):
        j_dict = json.loads(message)
        if "id" not in j_dict.keys():
            # the first message doesn't carry relevant information
            return
        self.save_pointing_direction(j_dict)
        self.print_gesture_type(j_dict)


    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        print("### closed ###")

    def on_open(self, ws):
        pass


if __name__ == "__main__":
    li = LeapInterface(url="ws://localhost:6437")
    li.start()