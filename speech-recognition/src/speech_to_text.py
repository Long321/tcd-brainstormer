import speech_recognition as sr

class stt_parser:
    def __init__(self, pause_treshold = 0.1):
        self.r = sr.Recognizer()
        self.mic = sr.Microphone()
        self.r.pause_treshold = pause_treshold

    # call this method to calibrate background noise
    # mult is a treshhold-multiplicator, increase means a louder input voice is required
    def adjust_noise_reduction(self, mult=1.3):
        with self.mic as source:
            print("Adjusting to background noise.")
            self.r.adjust_for_ambient_noise(source)
            self.r.energy_threshold *= mult
            print("New treshold: {}".format(self.r.energy_threshold))

    # activates speech recording, and returns word or phrase (string)
    # returns empty string if word was not recognized
    def get_word(self):
        with self.mic as source:
            audio = self.r.listen(source, phrase_time_limit=None)
            try:
                word = self.r.recognize_google(audio)
                return word
            except sr.UnknownValueError:
                return ""
            except sr.RequestError as e:
                print("Couldn't request results from Google Speech Recognition service; {0}".format(e))


def main():
    try:
        stt = stt_parser()
        stt.adjust_noise_reduction()
        while(True):
            print('Recording word.')
            value = stt.get_word()
            print(value)
    except KeyboardInterrupt:
        exit()


if __name__ == '__main__':
    main()


