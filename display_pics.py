# import pygame module in this program 
import pygame 
  
# activate the pygame library . 
# initiate pygame and give permission 
# to use pygame's functionality. 
pygame.init() 
  
# define the RGB value 
# for white colour 
white = (255, 255, 255) 
  
# assigning values to X and Y variable 
X = 1366
Y = 768

prev_size = 150  
# create the display surface object
# of specific dimension..e(X, Y). 
display_surface = pygame.display.set_mode((X, Y )) 
  
# set the pygame window name 
pygame.display.set_caption('TC Disrupt Demo') 
  
# create a surface object, image is drawn on it. 
image_one = pygame.image.load('MoCap/example_images/planet.png') 
image_one = pygame.transform.scale(image_one, (prev_size,prev_size))
image_two = pygame.image.load('MoCap/example_images/plant.png') 
image_two = pygame.transform.scale(image_two, (prev_size,prev_size))
image_three = pygame.image.load('MoCap/example_images/panda.png')
image_three = pygame.transform.scale(image_three, (prev_size,prev_size)) 

# infinite loop 
while True : 
  
    # completely fill the surface object 
    # with white colour 
    display_surface.fill(white) 
    font = pygame.font.SysFont("comicsansms", 32)
    text = font.render("Hello, Marius", True, (0, 0, 0))
    display_surface.blit(text,
        (320 - text.get_width() // 2, 240 - text.get_height() // 2))
    # copying the image surface object 
    # to the display surface object at 
    # (0, 0) coordinate. 
    display_surface.blit(image_one, (X // 2 + prev_size, 500))
    display_surface.blit(image_two, (X // 2 , 500))
    display_surface.blit(image_three, (X // 2 - 150, 500)) 
  
    # iterate over the list of Event objects 
    # that was returned by pygame.event.get() method. 
    for event in pygame.event.get() : 
  
        # if event object type is QUIT 
        # then quitting the pygame 
        # and program both. 
        if event.type == pygame.QUIT : 
  
            # deactivates the pygame library 
            pygame.quit() 
  
            # quit the program. 
            quit() 
  
        # Draws the surface object to the screen.   
        pygame.display.update() 