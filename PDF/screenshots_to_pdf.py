# add image to pdf
 
from fpdf import FPDF
 
def add_image(image_path):
    pdf = FPDF()
    pdf.add_page()
    pdf.image(image_path, x=10, y=10, w=100)
    pdf.output("add_images.pdf")
 
if __name__ == '__main__':
   # add_image('MoCap/example_images/panda.png')
    add_image('MoCap/example_images/planet.png')