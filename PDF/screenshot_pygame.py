import pygame
pygame.init()
screen = pygame.display.set_mode((500, 500))

X = 500
Y = 500

def screenshot(obj, file_name, position, size):
    img = pygame.Surface(size)
    img.blit(obj, (0, 0), (position, size))
    pygame.image.save(img, file_name)

screenshot(screen, "pygame_screenshot.png", (0, 0), (X, Y))
print("Screenshot taken")

pygame.display.update()