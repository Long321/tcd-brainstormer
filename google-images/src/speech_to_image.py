from google_images_download import google_images_download as gid

import sys
sys.path.append('../speech-recognition/src/')
from speech_to_text import stt_parser


# image limit is the amount of images / urls to download
class sti_parser(stt_parser):
    def __init__(self, img_limit=100):
        super().__init__()
        self.img_limit = img_limit
        self.response = gid.googleimagesdownload()

    # downloads google-images with keyword == word (string) into local folder
    def get_image_from_text(self, word):
        print('Searching images for: ' + word)
        arguments = {"keywords": word, "limit": 3, "print_urls": False, 'r ': 'labeled-for-nocommercial-reuse'}
        paths = self.response.download(arguments)  # passing the arguments to the function
        # print(paths)  # printing absolute paths of the downloaded images
        return paths

    # returns image-urls (string list) with keyword == word (string)
    def get_image_urls_from_text(self, word, offset=0):
        mock_response = gid.googleimagesdownload()
        print('Searching image-urls for: ' + word)
        arguments = {"keywords": word, "limit":self.img_limit,
                     "print_urls": False, 'r ':
                     'labeled-for-nocommercial-reuse',
                     'no_download': True,
                     'offset': offset * self.img_limit + 1,
                     'silent_mode': True
                     }
        paths = mock_response.download(arguments)  # passing the arguments to the function
        urls = paths[0][word]
        return urls

    # deprecated
    def refresh_urls(self, word, iter):
        self.get_image_url_from_text(word, iter)

    # downloads images from speech into local folder
    def get_image_from_speech(self):
        value = ""
        while value == "":
            value = self.get_word()
        self.get_image_from_text('value')

    def get_image_urls_from_speech(self):
        value = ""
        while value == "":
            value = self.get_word()
        return self.get_image_urls_from_text(value)


def main():
    try:
        sti = sti_parser(img_limit=100)
        word = 'banana'
        sti.adjust_noise_reduction()
        value = ""
        while value == '':
            print('recording word.')
            value = sti.get_word()
            print('.')
        sti.get_image_urls_from_text(value)
    except KeyboardInterrupt:
        exit()


if __name__ == '__main__':
    main()